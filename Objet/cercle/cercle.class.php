<?php

include('../point/point.class.php');

class Cercle{
    private $rayon;
    private Point $point ;
    
    
    
    public function __construct($rayon,$x,$y){
        $this->rayon=$rayon;
        $this->point= new Point($x,$y);

    }

    public function getPerimetre(){
        $peri= 2 * pi() * $this->rayon;
        return $peri;
    }
    public function getSurface()
    {
        $surface = pi() * $this->rayon**2;
        return $surface;
    }
    public function appartient(Point $p):void {

        $distance = sqrt((($this->point->getAbscisse() - $p->getAbscisse()) * ($this->point->getAbscisse() - $p->getAbscisse())) + (($this->point->getOrdonnee() - $p->getOrdonnee() ) * 
        ($this->point->getOrdonnee() - $p->getOrdonnee() )));
        if ($distance <= $this->rayon ) {
            echo "Le Point P appartient au cercle";
        }
        else {
            echo "Le Point P n'appartient pas au cercle";
        }
    
    
    }
    public function afficher()
    {
        echo "CERCLE (".$this->point->getAbscisse().", ".$this->point->getOrdonnee().", ".$this->rayon.")";
        echo PHP_EOL;
        echo "Le périmètre est : ".$this->getPerimetre();
        echo PHP_EOL;
        echo "La surface est : ".$this->getSurface();

    }
    


    /**
     * Get the value of rayon
     */ 
    public function getRayon()
    {
        return $this->rayon;
    }

    /**
     * Set the value of rayon
     *
     * @return  self
     */ 
    public function setRayon($rayon)
    {
        $this->rayon = $rayon;

        return $this;
    }

    /**
     * Get the value of point
     */ 
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Set the value of point
     *
     * @return  self
     */ 
    public function setPoint($point)
    {
        $this->point = $point;

        return $this;
    }
}