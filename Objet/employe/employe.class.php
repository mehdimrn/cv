<?php

class Employe{
private $matricule;
private $nom;
private $prenom;
private $dateNaissance;
private $dateEmbauche;
private $salaire;

public function __construct($matricule,$nom,$prenom,$dateNaissance,$dateEmbauche,$salaire)
{
   $this->matricule=$matricule;
   $this->nom=$nom;
   $this->prenom=$prenom;
   $this->dateNaissance=$dateNaissance;
   $this->dateEmbauche=$dateEmbauche;
   $this->salaire=$salaire;
}
public function age()
{
    $tab=explode('/',$this->dateNaissance);
    $jour=$tab[0];
    $mois=$tab[1];
    $annee=$tab[2];
    if ($jour>20 and $mois>7) {
        $age=2022-$annee-1;
        return $age;
    }else {
        $age=2022-$annee;
        return $age;
    }
    

}
public function anciennete()
{
    $tab2=explode('/',$this->dateEmbauche);
    $jour=$tab2[0];
    $mois=$tab2[1];
    $annee=$tab2[2];
    if ($jour>20 and $mois>7) {
        $anciennete=2022-$annee-1;
        return "la personne a ".$anciennete." ans d'anciennete dans l'entreprise";
    }else {
        $anciennete=2022-$annee;
        return "la personne a ".$anciennete." ans d'anciennete dans l'entreprise";
    }
    
}
public function augmentationSalaire()
{
    if ($anciennete<5) {
        $this->salaire=$this->salaire*0.02+$this->salaire;
        return $this->salaire;
    }elseif ($anciennete<10) {
        $this->salaire=$this->salaire*0.05+$this->salaire;
        return $this->salaire;
    }else {
        $this->salaire=$this->salaire*0.1+$this->salaire;
        return $this->salaire;
    }
}
public function afficherEmploye()
{   
    $age= $this->age();
    $anciennete= $this->anciennete();
    echo "Matricule : ".$this->matricule;
    echo PHP_EOL;
    echo"Nom Complet : ".$this->nom." ".$this->prenom;
    echo PHP_EOL;
    echo"Age : ".$age;
    echo PHP_EOL;
    echo"Anciennete : ".$anciennete;
    echo PHP_EOL;
    echo"Salaire : ".$this->salaire;
}
}