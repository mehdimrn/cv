<?php

class Rectangle{

    private $longueur;
    private $largeur;

    public function __construct($x,$y){
       
        $this->longueur = $x;
       
        $this->largeur = $y;
    }

    public function perimetre()
    {
       $perimetre= ($this->longueur*2) + ($this->largeur*2);
       return "le perimetre est de: ".$perimetre;
    }
    public function aire()
    {
        $aire= $this->longueur*$this->largeur;
        return "l'aire est de: ".$aire;
    }
    public function estCarre()
    {
        if ($longueur == $largeur) {
            return "c'est un carré !";
        }else {
            return "c'est un rectangle !";
        }

    }
    public function afficherRectangle()
    {
        $perimetre= $this->perimetre();
        $aire = $this->aire();
        if ($this->longueur == $this->largeur) {
            return "Longueur : [$this->longueur] - Largeur : [$this->largeur] - Périmètre : [".$perimetre."] - Aire : [".$aire."] - Il s’agit d’un carré ";
        }else {
            return "Longueur : [$this->longueur] - Largeur : [$this->largeur] - Périmètre : [".$perimetre."] - Aire : [".$aire."] -  Il ne s’agit pas d’un carré";
        }
        
    }






    /**
     * Get the value of longueur
     */ 
    public function getLongueur()
    {
        return $this->longueur;
    }

    /**
     * Set the value of longueur
     *
     * @return  self
     */ 
    public function setLongueur($longueur)
    {
        $this->longueur = $longueur;

        return $this;
    }

    /**
     * Get the value of largeur
     */ 
    public function getLargeur()
    {
        return $this->largeur;
    }

    /**
     * Set the value of largeur
     *
     * @return  self
     */ 
    public function setLargeur($largeur)
    {
        $this->largeur = $largeur;

        return $this;
    }
}