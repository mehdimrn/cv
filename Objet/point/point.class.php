<?php

class Point {
    private $abscisse;
    private $ordonnee;


    public function __construct($x,$y){
       
        $this->abscisse = $x;
       
        $this->ordonnee = $y;
    }
    public function norme(){
       
        $norme= sqrt(    ($this->abscisse**2) + ($this->ordonnee**2)    );
       
        return $norme;
    }public function afficher()
    {
        echo "POINT(".$this->abscisse.",".$this->ordonnee.")";
    }
    

    /**
     * Get the value of abscisse
     */ 
    public function getAbscisse()
    {
        return $this->abscisse;
    }

    /**
     * Set the value of abscisse
     *
     * @return  self
     */ 
    public function setAbscisse($abscisse)
    {
        $this->abscisse = $abscisse;

        return $this;
    }

    /**
     * Get the value of ordonne
     */ 
    public function getOrdonnee()
    {
        return $this->ordonnee;
    }

    /**
     * Set the value of ordonne
     *
     * @return  self
     */ 
    public function setOrdonne($ordonnee)
    {
        $this->ordonnee = $ordonnee;

        return $this;
    }
}