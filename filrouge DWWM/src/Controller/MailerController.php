<?php

namespace App\Controller;

use App\Form\MailerType;
use Symfony\Component\Mime\Email;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Profiler\Profile;
use Symfony\Component\Mailer\Transport\TransportInterface;

class MailerController extends AbstractController
{
    /**
     * @Route("/mailer", name="app_mailer")
     */
    public function index(Request $request, TransportInterface $mailer): Response
    {
        $form = $this->createForm(MailerType::class);

        $form->handleRequest($request);
        // verification du formulaire
        if ($form->isSubmitted() && $form->isValid()) {
            //recuperation des champs entré dans le formulaire SSI elles sont valides
            $data = $form->getData();
        // on les stock dans des variables
            $monadresseemail = $data['from'];
            $adresseclient = $data['to'];
            $objet = $data['objet'];
            $content = $data['content'];
            $fichier = $data['fichier'];

        // instanciation de l'objet email avec notre nouvelle class mailer
            $email = (new Email())
                // mail de l'expediteur
                ->from($monadresseemail)
                // mail du destinataire
                ->to($adresseclient)
                // les informations du mail
                ->subject($objet)
                ->text($content)
                ->attachFromPath($fichier);
            // pour envoyer l'email
            $mailer->send($email);
        }

        return $this->renderForm('mailer/index.html.twig', [
            'formulaire' => $form
        ]);
    }
}
