<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Entity\Comment;
use App\Form\ArticlesType;
use App\Form\CommentType;
use App\Repository\ArticlesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogController extends AbstractController
{
    // Acceuil
    /**
     * @Route("/",name="home")
     */
    public function home()
    {
        return $this->render(
            'blog/home.html.twig',
            [
                'title' => 'Bienvenue chez PROPAR',
            ]
        );
    }

    // Liste des articles
    /**
     * @Route("/blog", name="app_blog")
     */
    public function index(ArticlesRepository $articlesRepository ): Response
    {
        /*
            function index(ArticlesRepository $articlesRepository) => est une injection de dépendance car la fonction index à besoin des données de la bdd qui sont dans Repository/ArticleRepository.php 
        */
        $articles = $articlesRepository->findAll();

        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
            'articles' => $articles
        ]);
    }

    // Ajouter un article
    /**
     * @Route("/blog/newArticle", name="newArticle")
     * @Route("/blog/{id}/edit", name="blog_edit")
     */
    public function form(Articles $article = null, Request $request, EntityManagerInterface $manager): Response
    {
        // dump($request);
        
        if(!$article) {
            // Crée un article vide
            $article = new Articles();
        }

        // $form = $this->createFormBuilder($article)
        //         ->add('title')
        //         ->add('content')
        //         ->add('image')
        //         ->getForm();

        // Avec la creation automatique du formulaire
        $form = $this->createForm(ArticlesType::class, $article);

        $form->handleRequest($request);

        /* 
            Si le form est soumis et que les données sont OK enregistre dans la bdd
            et redirige moi a la page /blog/article/{id}
        */
        if ($form->isSubmitted() && $form->isValid()) {

            if (!$article->getId()) {

                $article->setCreatedAt(new \DateTime());
            }

            $manager->persist($article);
            $manager->flush();

            return $this->redirectToRoute('blog_show', ['id' => $article->getId()]);
        }

        /* 
            la variable editMode donne l'état l'article
            -> false si l'id de l'article n'existe pas et vice-versa
            -> l'affichage se fait dans le blog/addArticle.html.twig

        */
        return $this->render('blog/addArticle.html.twig', [
                'form_article' => $form->createView(),
                'editMode' => $article->getId() !== null,
            ]
        );
    }

    // Afficher un article precis
    /**
     * @Route("/blog/article/{id}", name="blog_show")
     * 
     */
    // public function show(ArticlesRepository $articlesRepository, int $id, Request $request, EntityManagerInterface $manager)
    public function show(Articles $article, Request $request, EntityManagerInterface $manager)
    {

        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $comment->setCreatedAt(new \DateTime())
                    ->setArticle($article);
            $manager->persist($comment);
            $manager->flush();

            return $this->redirectToRoute('blog_show', ['id' => $article->getId()]);
        }

        // $article = $articlesRepository->find($id);

        return $this->render('blog/show.html.twig',
            [
                'article' => $article,
                'commentForm' => $form->createView()
            ]
        );
    }
}
