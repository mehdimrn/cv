<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Articles;
use App\Entity\Category;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // Remplir notre BDD (faker => package pour remplir les données au hasard)
        $faker = \Faker\Factory::create('fr_FR');

        // Créer 3 catégories avec faker
        for ($i = 0; $i  <= 3; $i ++) { 

            $category = new Category();
            $category->setTitle($faker->sentence())
                     ->setDescription($faker->paragraph());

            $manager->persist($category);

            // Créer entre 4 et 6 articles
            for ($j = 1; $j <= rand(4, 6); $j++){ 
    
                // $faker->paragraphs() => retourne un tableau d'ou
                $content = implode('</p><p>', $faker->paragraphs(5));
                $content = '<p>' . $content . '</p>';

                $article = new Articles();
                $article->setTitle($faker->sentence())
                        ->setContent($content)
                        ->setImage($faker->imageUrl())
                        ->setCreatedAt($faker->dateTimeBetween('-6 months'))
                        ->setCategory($category);
    
                $manager->persist($article);

                // On donne des commentaires (entre 4 et 10) à l'article
                for ($k = 0; $k  < rand(4, 10); $k ++) { 
                    
                    $comment = new Comment();

                    $content = implode('</p><p>', $faker->paragraphs(2));
                    $content = '<p>' . $content . '</p>';

                    // NB: Date de commentaire est entre dateCreationArticle et dateNow
                    $now = new \DateTime();
                    $interval = $now->diff(($article->getCreatedAt()))->format('%R%a days');

                    $comment->setAuthor($faker->name)
                            ->setContent($content)
                            ->setCreatedAt($faker->dateTimeBetween($interval))
                            ->setArticle($article);

                    $manager->persist($comment);
                }
            }
        }
        $manager->flush();
    }
}
