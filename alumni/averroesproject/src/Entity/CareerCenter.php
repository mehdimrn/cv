<?php

namespace App\Entity;

use App\Repository\CareerCenterRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CareerCenterRepository::class)]
class CareerCenter
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $typeAnn = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $descAnn = null;

    #[ORM\Column(length: 255)]
    private ?string $levelAnn = null;

    #[ORM\Column(length: 255)]
    private ?string $durationAnn = null;

    #[ORM\Column(length: 255)]
    private ?string $contactAnn = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $dateAnn = null;

    #[ORM\Column(length: 255)]
    private ?string $rhythmeAnn = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $startAnn = null;

    #[ORM\Column(length: 255)]
    private ?string $sectorAnn = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $PDF = null;

    #[ORM\ManyToOne(inversedBy: 'careerCenter')]
    private ?Profil $profil = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeAnn(): ?string
    {
        return $this->typeAnn;
    }

    public function setTypeAnn(string $typeAnn): self
    {
        $this->typeAnn = $typeAnn;

        return $this;
    }

    public function getDescAnn(): ?string
    {
        return $this->descAnn;
    }

    public function setDescAnn(string $descAnn): self
    {
        $this->descAnn = $descAnn;

        return $this;
    }

    public function getLevelAnn(): ?string
    {
        return $this->levelAnn;
    }

    public function setLevelAnn(string $levelAnn): self
    {
        $this->levelAnn = $levelAnn;

        return $this;
    }

    public function getDurationAnn(): ?string
    {
        return $this->durationAnn;
    }

    public function setDurationAnn(string $durationAnn): self
    {
        $this->durationAnn = $durationAnn;

        return $this;
    }

    public function getContactAnn(): ?string
    {
        return $this->contactAnn;
    }

    public function setContactAnn(string $contactAnn): self
    {
        $this->contactAnn = $contactAnn;

        return $this;
    }

    public function getDateAnn(): ?\DateTimeInterface
    {
        return $this->dateAnn;
    }

    public function setDateAnn(\DateTimeInterface $dateAnn): self
    {
        $this->dateAnn = $dateAnn;

        return $this;
    }

    public function getRhythmeAnn(): ?string
    {
        return $this->rhythmeAnn;
    }

    public function setRhythmeAnn(string $rhythmeAnn): self
    {
        $this->rhythmeAnn = $rhythmeAnn;

        return $this;
    }

    public function getStartAnn(): ?\DateTimeInterface
    {
        return $this->startAnn;
    }

    public function setStartAnn(\DateTimeInterface $startAnn): self
    {
        $this->startAnn = $startAnn;

        return $this;
    }

    public function getSectorAnn(): ?string
    {
        return $this->sectorAnn;
    }

    public function setSectorAnn(string $sectorAnn): self
    {
        $this->sectorAnn = $sectorAnn;

        return $this;
    }

    public function getPDF(): ?string
    {
        return $this->PDF;
    }

    public function setPDF(?string $PDF): self
    {
        $this->PDF = $PDF;

        return $this;
    }

    public function getProfil(): ?Profil
    {
        return $this->profil;
    }

    public function setProfil(?Profil $profil): self
    {
        $this->profil = $profil;

        return $this;
    }

    public function __toString()
    {
        return $this->id;
    }
}
