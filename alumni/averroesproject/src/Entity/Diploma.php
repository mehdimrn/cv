<?php

namespace App\Entity;

use App\Repository\DiplomaRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DiplomaRepository::class)]
class Diploma
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $yearDiploma = null;

    #[ORM\Column(length: 255)]
    private ?string $descDiploma = null;

    #[ORM\Column(length: 255)]
    private ?string $linkDiploma = null;

    #[ORM\Column(length: 255)]
    private ?string $School = null;

    #[ORM\Column(length: 255)]
    private ?string $levelDiploma = null;

    #[ORM\Column(length: 255)]
    private ?string $typeDiploma = null;

    #[ORM\ManyToOne(inversedBy: 'diploma')]
    private ?Profil $profil = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getYearDiploma(): ?string
    {
        return $this->yearDiploma;
    }

    public function setYearDiploma(string $yearDiploma): self
    {
        $this->yearDiploma = $yearDiploma;

        return $this;
    }

    public function getDescDiploma(): ?string
    {
        return $this->descDiploma;
    }

    public function setDescDiploma(string $descDiploma): self
    {
        $this->descDiploma = $descDiploma;

        return $this;
    }

    public function getLinkDiploma(): ?string
    {
        return $this->linkDiploma;
    }

    public function setLinkDiploma(string $linkDiploma): self
    {
        $this->linkDiploma = $linkDiploma;

        return $this;
    }

    public function getSchool(): ?string
    {
        return $this->School;
    }

    public function setSchool(string $School): self
    {
        $this->School = $School;

        return $this;
    }

    public function getLevelDiploma(): ?string
    {
        return $this->levelDiploma;
    }

    public function setLevelDiploma(string $levelDiploma): self
    {
        $this->levelDiploma = $levelDiploma;

        return $this;
    }

    public function getTypeDiploma(): ?string
    {
        return $this->typeDiploma;
    }

    public function setTypeDiploma(string $typeDiploma): self
    {
        $this->typeDiploma = $typeDiploma;

        return $this;
    }

    public function getProfil(): ?Profil
    {
        return $this->profil;
    }

    public function setProfil(?Profil $profil): self
    {
        $this->profil = $profil;

        return $this;
    }

    public function __toString()
    {
        return $this->id;
    }
}
