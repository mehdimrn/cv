<?php

namespace App\Entity;

use Datetime;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\EventRepository;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

#[ORM\Entity(repositoryClass: EventRepository::class)]
class Event
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $titleEvent = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $descEvent = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?DateTimeInterface $DatePostEvent = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private ?\DateTimeInterface $dateEvent = null;

    #[ORM\Column(length: 255)]
    private ?string $typeEvent = null;

    #[ORM\Column(length: 255)]
    private ?string $placeEvent = null;

    #[ORM\Column]
    private ?int $numberOfSitEvent = null;

    #[ORM\Column(length: 255)]
    private ?string $contactEventOwner = null;

    #[ORM\Column(length: 255)]
    private ?bool $statusEvent = null;

    #[ORM\Column(length: 255)]
    private ?string $animatorEvent = null;

    #[ORM\Column(length: 255)]
    private ?string $durationEvent = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $PDF = null;

    #[ORM\Column]
    private ?int $publicPrivateEvent = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $restricted = null;

    #[ORM\Column]
    private ?int $freeEvent = null;

    #[ORM\ManyToOne(inversedBy: 'event')]
    private ?Profil $profil = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitleEvent(): ?string
    {
        return $this->titleEvent;
    }

    public function setTitleEvent(string $titleEvent): self
    {
        $this->titleEvent = $titleEvent;

        return $this;
    }

    public function getDescEvent(): ?string
    {
        return $this->descEvent;
    }

    public function setDescEvent(string $descEvent): self
    {
        $this->descEvent = $descEvent;

        return $this;
    }

    public function getDatePostEvent(): ?\DateTimeInterface
    {
        return $this->DatePostEvent;
    }

    public function setDatePostEvent(\DateTimeInterface $DatePostEvent): self
    {
        $this->DatePostEvent = $DatePostEvent;

        return $this;
    }

    public function __construct(){

        $this->DatePostEvent= new \Datetime();
    }

    public function getDateEvent(): ?\DateTimeInterface
    {
        return $this->dateEvent;
    }

    public function setDateEvent(\DateTimeInterface $dateEvent): self
    {
        $this->dateEvent = $dateEvent;

        return $this;
    }

    public function getTypeEvent(): ?string
    {
        return $this->typeEvent;
    }

    public function setTypeEvent(string $typeEvent): self
    {
        $this->typeEvent = $typeEvent;

        return $this;
    }

    public function getPlaceEvent(): ?string
    {
        return $this->placeEvent;
    }

    public function setPlaceEvent(string $placeEvent): self
    {
        $this->placeEvent = $placeEvent;

        return $this;
    }

    public function getNumberOfSitEvent(): ?int
    {
        return $this->numberOfSitEvent;
    }

    public function setNumberOfSitEvent(int $numberOfSitEvent): self
    {
        $this->numberOfSitEvent = $numberOfSitEvent;

        return $this;
    }

    public function getContactEventOwner(): ?string
    {
        return $this->contactEventOwner;
    }

    public function setContactEventOwner(string $contactEventOwner): self
    {
        $this->contactEventOwner = $contactEventOwner;

        return $this;
    }

    public function getStatusEvent(): ?string
    {
        return $this->statusEvent;
    }

    public function setStatusEvent(string $statusEvent): self
    {
        $this->statusEvent = $statusEvent;

        return $this;
    }

    public function getAnimatorEvent(): ?string
    {
        return $this->animatorEvent;
    }

    public function setAnimatorEvent(string $animatorEvent): self
    {
        $this->animatorEvent = $animatorEvent;

        return $this;
    }

    public function getDurationEvent(): ?string
    {
        return $this->durationEvent;
    }

    public function setDurationEvent(string $durationEvent): self
    {
        $this->durationEvent = $durationEvent;

        return $this;
    }

    public function getPDF(): ?string
    {
        return $this->PDF;
    }

    public function setPDF(?string $PDF): self
    {
        $this->PDF = $PDF;

        return $this;
    }

    public function getPublicPrivateEvent(): ?int
    {
        return $this->publicPrivateEvent;
    }

    public function setPublicPrivateEvent(int $publicPrivateEvent): self
    {
        $this->publicPrivateEvent = $publicPrivateEvent;

        return $this;
    }

    public function getRestricted(): ?string
    {
        return $this->restricted;
    }

    public function setRestricted(?string $restricted): self
    {
        $this->restricted = $restricted;

        return $this;
    }

    public function getFreeEvent(): ?int
    {
        return $this->freeEvent;
    }

    public function setFreeEvent(int $freeEvent): self
    {
        $this->freeEvent = $freeEvent;

        return $this;
    }

    public function getProfil(): ?Profil
    {
        return $this->profil;
    }

    public function setProfil(?Profil $profil): self
    {
        $this->profil = $profil;

        return $this;
    }

    public function __toString()
    {
        return $this->id;
    }
}
