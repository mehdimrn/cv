<?php

namespace App\Entity;

use App\Repository\CandidacyCenterRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CandidacyCenterRepository::class)]
class CandidacyCenter
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $typeCandidacy = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $descCandidacy = null;

    #[ORM\Column(length: 255)]
    private ?string $levelCandidacy = null;

    #[ORM\Column(length: 255)]
    private ?string $durationCandidacy = null;

    #[ORM\Column(length: 255)]
    private ?string $contactCandidacy = null;

    #[ORM\Column(length: 255)]
    private ?string $schoolCandidacy = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $dateCandidacy = null;

    #[ORM\Column(length: 255)]
    private ?string $rhythmeCandidacy = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $startCandidacy = null;

    #[ORM\Column(length: 255)]
    private ?string $sectorCandidacy = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $PDF = null;

    #[ORM\ManyToOne(inversedBy: 'candidacyCenter')]
    private ?Profil $profil = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeCandidacy(): ?string
    {
        return $this->typeCandidacy;
    }

    public function setTypeCandidacy(string $typeCandidacy): self
    {
        $this->typeCandidacy = $typeCandidacy;

        return $this;
    }

    public function getDescCandidacy(): ?string
    {
        return $this->descCandidacy;
    }

    public function setDescCandidacy(string $descCandidacy): self
    {
        $this->descCandidacy = $descCandidacy;

        return $this;
    }

    public function getLevelCandidacy(): ?string
    {
        return $this->levelCandidacy;
    }

    public function setLevelCandidacy(string $levelCandidacy): self
    {
        $this->levelCandidacy = $levelCandidacy;

        return $this;
    }

    public function getDurationCandidacy(): ?string
    {
        return $this->durationCandidacy;
    }

    public function setDurationCandidacy(string $durationCandidacy): self
    {
        $this->durationCandidacy = $durationCandidacy;

        return $this;
    }

    public function getContactCandidacy(): ?string
    {
        return $this->contactCandidacy;
    }

    public function setContactCandidacy(string $contactCandidacy): self
    {
        $this->contactCandidacy = $contactCandidacy;

        return $this;
    }

    public function getSchoolCandidacy(): ?string
    {
        return $this->schoolCandidacy;
    }

    public function setSchoolCandidacy(string $schoolCandidacy): self
    {
        $this->schoolCandidacy = $schoolCandidacy;

        return $this;
    }

    public function getDateCandidacy(): ?\DateTimeInterface
    {
        return $this->dateCandidacy;
    }

    public function setDateCandidacy(\DateTimeInterface $dateCandidacy): self
    {
        $this->dateCandidacy = $dateCandidacy;

        return $this;
    }

    public function getRhythmeCandidacy(): ?string
    {
        return $this->rhythmeCandidacy;
    }

    public function setRhythmeCandidacy(string $rhythmeCandidacy): self
    {
        $this->rhythmeCandidacy = $rhythmeCandidacy;

        return $this;
    }

    public function getStartCandidacy(): ?\DateTimeInterface
    {
        return $this->startCandidacy;
    }

    public function setStartCandidacy(\DateTimeInterface $startCandidacy): self
    {
        $this->startCandidacy = $startCandidacy;

        return $this;
    }

    public function getSectorCandidacy(): ?string
    {
        return $this->sectorCandidacy;
    }

    public function setSectorCandidacy(string $sectorCandidacy): self
    {
        $this->sectorCandidacy = $sectorCandidacy;

        return $this;
    }

    public function getPDF(): ?string
    {
        return $this->PDF;
    }

    public function setPDF(?string $PDF): self
    {
        $this->PDF = $PDF;

        return $this;
    }

    public function getProfil(): ?Profil
    {
        return $this->profil;
    }

    public function setProfil(?Profil $profil): self
    {
        $this->profil = $profil;

        return $this;
    }
    
    public function __toString()
    {
        return $this->id;
    }
}
