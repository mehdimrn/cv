<?php

namespace App\Entity;

use App\Repository\InformationsProfilRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: InformationsProfilRepository::class)]
class InformationsProfil
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $jobsTitle = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $activitySector = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $linkJobCard = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $linkedin = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $linkCompany = null;

    #[ORM\OneToOne(targetEntity: Profil::class, cascade: ['persist', 'remove'])]
    private $profil;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJobsTitle(): ?string
    {
        return $this->jobsTitle;
    }

    public function setJobsTitle(?string $jobsTitle): self
    {
        $this->jobsTitle = $jobsTitle;

        return $this;
    }

    public function getActivitySector(): ?string
    {
        return $this->activitySector;
    }

    public function setActivitySector(?string $activitySector): self
    {
        $this->activitySector = $activitySector;

        return $this;
    }

    public function getLinkJobCard(): ?string
    {
        return $this->linkJobCard;
    }

    public function setLinkJobCard(?string $linkJobCard): self
    {
        $this->linkJobCard = $linkJobCard;

        return $this;
    }

    public function getLinkedin(): ?string
    {
        return $this->linkedin;
    }

    public function setLinkedin(?string $linkedin): self
    {
        $this->linkedin = $linkedin;

        return $this;
    }

    public function getLinkCompany(): ?string
    {
        return $this->linkCompany;
    }

    public function setLinkCompany(?string $linkCompany): self
    {
        $this->linkCompany = $linkCompany;

        return $this;
    }

    public function getProfil(): ?Profil
    {
        return $this->profil;
    }



    public function __toString()
    {
        return $this->id;
    }

    public function setProfil(?Profil $profil): self
    {
        $this->profil = $profil;

        return $this;
    }
}
