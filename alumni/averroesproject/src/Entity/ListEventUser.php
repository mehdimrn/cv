<?php

namespace App\Entity;

use App\Repository\ListEventUserRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ListEventUserRepository::class)]
class ListEventUser
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?int $idAccount = null;

    #[ORM\Column]
    private ?int $numberUserPublic = null;

    #[ORM\ManyToOne]
    private ?Event $Event = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdAccount(): ?string
    {
        return $this->idAccount;
    }

    public function setIdAccount(string $idAccount): self
    {
        $this->idAccount = $idAccount;

        return $this;
    }

    public function getNumberUserPublic(): ?int
    {
        return $this->numberUserPublic;
    }

    public function setNumberUserPublic(int $numberUserPublic): self
    {
        $this->numberUserPublic = $numberUserPublic;

        return $this;
    }

    public function __toString()
    {
        return $this->id;
    }

    public function getEvent(): ?Event
    {
        return $this->Event;
    }

    public function setEvent(?Event $Event): self
    {
        $this->Event = $Event;

        return $this;
    }
}
