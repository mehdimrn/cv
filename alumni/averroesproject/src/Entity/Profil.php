<?php

namespace App\Entity;

use App\Repository\ProfilRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProfilRepository::class)]
class Profil
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $lastname = null;

    #[ORM\Column(length: 255)]
    private ?string $firstname = null;

    #[ORM\Column(length: 255)]
    private ?string $phoneNumber = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $yearBac = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $studentClass = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Account $account = null;

    #[ORM\OneToMany(mappedBy: 'profil', targetEntity: Diploma::class)]
    private Collection $diploma;

    #[ORM\OneToMany(mappedBy: 'profil', targetEntity: CareerCenter::class)]
    private Collection $careerCenter;

    #[ORM\OneToMany(mappedBy: 'profil', targetEntity: CandidacyCenter::class)]
    private Collection $candidacyCenter;

    #[ORM\OneToMany(mappedBy: 'profil', targetEntity: Event::class)]
    private Collection $event;

    #[ORM\OneToMany(mappedBy: 'profil', targetEntity: Competences::class)]
    private Collection $competences;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $color = null;



    public function __construct()
    {
        $this->diploma = new ArrayCollection();
        $this->careerCenter = new ArrayCollection();
        $this->candidacyCenter = new ArrayCollection();
        $this->event = new ArrayCollection();
        $this->competences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastName(): ?string
    {
        return $this->lastname;
    }

    public function setLastName(string $name): self
    {
        $this->lastname = $name;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getYearBac(): ?string
    {
        return $this->yearBac;
    }

    public function setYearBac(?string $yearBac): self
    {
        $this->yearBac = $yearBac;

        return $this;
    }

    public function getStudentClass(): ?string
    {
        return $this->studentClass;
    }

    public function setStudentClass(?string $studentClass): self
    {
        $this->studentClass = $studentClass;

        return $this;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @return Collection<int, Diploma>
     */
    public function getDiploma(): Collection
    {
        return $this->diploma;
    }

    public function addDiploma(Diploma $diploma): self
    {
        if (!$this->diploma->contains($diploma)) {
            $this->diploma->add($diploma);
            $diploma->setProfil($this);
        }

        return $this;
    }

    public function removeDiploma(Diploma $diploma): self
    {
        if ($this->diploma->removeElement($diploma)) {
            // set the owning side to null (unless already changed)
            if ($diploma->getProfil() === $this) {
                $diploma->setProfil(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CareerCenter>
     */
    public function getCareerCenter(): Collection
    {
        return $this->careerCenter;
    }

    public function addCareerCenter(CareerCenter $careerCenter): self
    {
        if (!$this->careerCenter->contains($careerCenter)) {
            $this->careerCenter->add($careerCenter);
            $careerCenter->setProfil($this);
        }

        return $this;
    }

    public function removeCareerCenter(CareerCenter $careerCenter): self
    {
        if ($this->careerCenter->removeElement($careerCenter)) {
            // set the owning side to null (unless already changed)
            if ($careerCenter->getProfil() === $this) {
                $careerCenter->setProfil(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CandidacyCenter>
     */
    public function getCandidacyCenter(): Collection
    {
        return $this->candidacyCenter;
    }

    public function addCandidacyCenter(CandidacyCenter $candidacyCenter): self
    {
        if (!$this->candidacyCenter->contains($candidacyCenter)) {
            $this->candidacyCenter->add($candidacyCenter);
            $candidacyCenter->setProfil($this);
        }

        return $this;
    }

    public function removeCandidacyCenter(CandidacyCenter $candidacyCenter): self
    {
        if ($this->candidacyCenter->removeElement($candidacyCenter)) {
            // set the owning side to null (unless already changed)
            if ($candidacyCenter->getProfil() === $this) {
                $candidacyCenter->setProfil(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Event>
     */
    public function getEvent(): Collection
    {
        return $this->event;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->event->contains($event)) {
            $this->event->add($event);
            $event->setProfil($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->event->removeElement($event)) {
            // set the owning side to null (unless already changed)
            if ($event->getProfil() === $this) {
                $event->setProfil(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Competences>
     */
    public function getCompetences(): Collection
    {
        return $this->competences;
    }

    public function addCompetence(Competences $competence): self
    {
        if (!$this->competences->contains($competence)) {
            $this->competences->add($competence);
            $competence->setProfil($this);
        }

        return $this;
    }

    public function removeCompetence(Competences $competence): self
    {
        if ($this->competences->removeElement($competence)) {
            // set the owning side to null (unless already changed)
            if ($competence->getProfil() === $this) {
                $competence->setProfil(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->id;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }
}
