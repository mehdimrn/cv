<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\Profil;
use App\Form\EventType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Form\FormInterface;
use DateTimeImmutable;



class EventController extends AbstractController
{
    #[Route('/event/create', name: 'event_create')]
    public function create(Request $request,EntityManagerInterface $entityManager): Response
    {
        //creation d'un nouvel event
    
        $event = new Event();
        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);

        //si le formulaire est correctement rempli on entre dans le IF
        if ($form->isSubmitted() && $form->isValid()) {

      

            // On recupere le profil de la personne connecter 
            $acc = $this->getUser()->getId();
            $profilUser = $entityManager->getRepository("App\Entity\Profil")->findOneBy(array('account' => $acc));

           //on set le bon profil et on set la date actuelle
            $event->setProfil($profilUser)
                  ->setDatePostEvent(new \DateTime())
                  ->setStatusEvent(0);

            
            // on envoie dans la bdd
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($event);
            $entityManager->flush();

            // on refresh la page
             return $this->redirectToRoute('event_create');
        }

        //on recupere le profil de la personne connecter
        $monId = $this->getUser()->getId();
        $Monprofil = $entityManager->getRepository("App\Entity\Profil")->findOneBy(array('account' => $monId));

        //on recupere les evenements creer par la personne connecter
        $MesEvent = $entityManager->getRepository("App\Entity\Event")->findBy(array('profil' => $Monprofil));

        return $this->render('event/create.html.twig', [
            'form' => $form->createView(),
            'MesEvent'=> $MesEvent,
            
        ]);
    }

    //bouton pour supprimer un event 
    #[Route('/event/delete/{id}', name: 'event_delete', methods: ['POST'])]
    public function delete(Event $event, EntityManagerInterface $entityManager): RedirectResponse
    {
        $entityManager->remove($event);
        $entityManager->flush();

        return $this->redirectToRoute('event_create');
    }


}
