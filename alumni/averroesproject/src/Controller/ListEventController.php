<?php

namespace App\Controller;

use App\Entity\Event;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListEventController extends AbstractController
{
    #[Route('/event/listEvent', name: 'list_event')]

    public function index(): Response
    {
        $events = $this->getDoctrine()->getRepository(Event::class)->findAll();



        
        return $this->render('event/listEvent.html.twig', [
            'events' => $events,
        ]);
    }
}

