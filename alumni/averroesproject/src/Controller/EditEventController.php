<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\Account;
use App\Form\EventType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditEventController extends AbstractController
{
    #[Route('/event/edit/{id}', name: 'event_edit', methods: ['GET', 'POST'])]
    public function index(Event $event, Request $request, EntityManagerInterface $entityManager): Response
    {

        $monId = $this->getUser()->getId();
        $monProfil = $entityManager->getRepository("App\Entity\Profil")->findOneBy(array('account' => $monId));

        $idProfil = $event->getProfil();
        
        
if ($monProfil == $idProfil) {
    


        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);


    
        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($event);
            $entityManager->flush();
    
            return $this->redirectToRoute('event_create');
        }
  

        return $this->render('event/edit.html.twig', [
            'form' => $form->createView(),
            // 'monIdUser' => $monIdUser,
            // 'idEvent' => $idEvent,
        ]);
    } else {
        return $this->redirectToRoute('event_create');

    }

    }
    
}