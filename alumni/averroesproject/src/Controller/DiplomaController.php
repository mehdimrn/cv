<?php

namespace App\Controller;

use App\Entity\Diploma;
use App\Form\DiplomaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\ORM\EntityManagerInterface;

class DiplomaController extends AbstractController
{
    #[Route('/diploma', name: 'app_diploma')]
    public function index(Request $request,EntityManagerInterface $entityManager): Response
    {

        $diploma = new Diploma();
        $form = $this->createForm(DiplomaType::class, $diploma);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // On recupere le profil de la personne connecter 
            $acc = $this->getUser()->getId();
            $profilUser = $entityManager->getRepository("App\Entity\Profil")->findOneBy(array('account' => $acc));

           //on set le bon profil et on set la date actuelle
            $diploma->setProfil($profilUser);
            // on envoie dans la bdd
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($diploma);
            $entityManager->flush();

            // on refresh la page
            return $this->redirectToRoute('app_diploma');
            
        }
            //on recupere le profil de la personne connecter
            $monId = $this->getUser()->getId();
            $Monprofil = $entityManager->getRepository("App\Entity\Profil")->findOneBy(array('account' => $monId));

            //on recupere les evenements creer par la personne connecter
            $MesDiplomes = $entityManager->getRepository("App\Entity\Diploma")->findBy(array('profil' => $Monprofil));




        return $this->render('diploma/index.html.twig', [
            'form' => $form->createView(),
            'MesDiplomes' => $MesDiplomes,
        ]);
    }

    //bouton pour supprimer un diplome
    #[Route('/diplome/delete/{id}', name: 'diploma_delete', methods: ['POST'])]
    public function delete(Diploma $diploma, EntityManagerInterface $entityManager): RedirectResponse
    {
        $entityManager->remove($diploma);
        $entityManager->flush();

        return $this->redirectToRoute('app_diploma');
    }
}
