<?php

namespace App\Controller;

use App\Form\InformationProfilType;
use App\Entity\InformationsProfil;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request as HttpFoundationRequest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InformationProfilController extends AbstractController
{
    #[Route('/information/profil', name: 'app_information_profil')]
    public function index(HttpFoundationRequest $request,EntityManagerInterface $entityManager): Response
    {

        $info = new InformationsProfil();
        $form = $this->createForm(InformationProfilType::class, $info);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            // On recupere le profil de la personne connecter 
            $acc = $this->getUser()->getId();
            $profilUser = $entityManager->getRepository("App\Entity\Profil")->findOneBy(array('account' => $acc));

           //on set le bon profil et on set la date actuelle
            $info->setProfil($profilUser);
            // on envoie dans la bdd
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($info);
            $entityManager->flush();


            // on refresh la page
            return $this->redirectToRoute('app_dashboard');

        }


        return $this->render('information_profil/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    #[Route('/information/profil/edit', name: 'app_information_profil_edit')]
            public function edit(HttpFoundationRequest $request, EntityManagerInterface $entityManager): Response
        {
    // Récupérer le profil de la personne connectée
    $acc = $this->getUser()->getId();
    $profilUser = $entityManager->getRepository("App\Entity\Profil")->findOneBy(['account' => $acc]);

    // Récupérer les informations de profil existantes
    $info = $entityManager->getRepository("App\Entity\InformationsProfil")->findOneBy(array('profil' => $profilUser));

    // Créer le formulaire avec les informations existantes
    $form = $this->createForm(InformationProfilType::class, $info);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        // Enregistrer les modifications dans la base de données
        $entityManager->persist($info);
        $entityManager->flush();

        // Rediriger vers la page de dashboard
        return $this->redirectToRoute('app_dashboard');
    }
    
    return $this->render('information_profil/edit.html.twig', [
        'form' => $form->createView(),
    ]);
}


}
