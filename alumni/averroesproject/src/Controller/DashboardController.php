<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Profil;
use App\Entity\Diploma;
use App\Entity\Competences;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class DashboardController extends AbstractController
{
    #[Route('/dashboard', name: 'app_dashboard')]
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {
        $myrole = $this->getUser()->getRoles();
        $myid = $this->getUser()->getId();
        $myemail = $this->getUser()->getEmail();

        $monId = $this->getUser()->getId();
        $Monprofil = $entityManager->getRepository("App\Entity\Profil")->findOneBy(array('account' => $monId));



        // Récupérer les informations de profil existantes
        $infoProfil = $entityManager->getRepository("App\Entity\InformationsProfil")->findOneBy(array('profil' => $Monprofil));

        //on recupere les diplomes creer par la personne connecter
        $MesDiplomes = $entityManager->getRepository("App\Entity\Diploma")->findBy(array('profil' => $Monprofil));

        //on recupere les competences creer par la personne connecter
        $MesCompetences = $entityManager->getRepository("App\Entity\Competences")->findBy(array('profil' => $Monprofil));


        return $this->render('dashboard/index.html.twig', [
            'myrole' => $myrole[0],
            'myid' => $myid,
            'myemail' => $myemail,
            'monprofil' => $Monprofil,
            'infoProfil' => $infoProfil,
            'mesdiplomes' => $MesDiplomes,
            'mescompetences' => $MesCompetences,
        ]);
    }



    #[Route('/competence/{id}/delete', name: 'app_delete_competence')]
    public function deleteCompetence(Request $request, EntityManagerInterface $entityManager, $id): Response
    {
        // Récupérer la compétence à supprimer
        $competence = $entityManager->getRepository("App\Entity\Competences")->find($id);

        // Vérifier si la compétence existe
        if (!$competence) {
            throw $this->createNotFoundException('La compétence n\'existe pas.');
        }

        // Supprimer la compétence
        $entityManager->remove($competence);
        $entityManager->flush();

        // Rediriger vers la page du tableau de bord ou une autre page après la suppression
        return $this->redirectToRoute('app_dashboard');
    }


    #[Route('/diploma/{id}/delete', name: 'app_delete_diploma')]
    public function deleteDiploma(Request $request, EntityManagerInterface $entityManager, $id): Response
    {
        // Récupérer le diplôme à supprimer
        $diploma = $entityManager->getRepository("App\Entity\Diploma")->find($id);

        // Vérifier si le diplôme existe
        if (!$diploma) {
            throw $this->createNotFoundException('Le diplôme n\'existe pas.');
        }

        // Supprimer le diplôme
        $entityManager->remove($diploma);
        $entityManager->flush();

        // Rediriger vers la page du tableau de bord ou une autre page après la suppression
        return $this->redirectToRoute('app_dashboard');
    }

    #[Route('/profil/{id}/select-color/{color}', name: 'app_select_color')]
    public function selectColor(Request $request, EntityManagerInterface $entityManager, Profil $profil, string $color): RedirectResponse
    {
        // Update the color in the Profil entity
        $profil->setColor($color);

        // Save the changes to the database
        $entityManager->persist($profil);
        $entityManager->flush();

        // Redirect back to the dashboard or any other page
        return $this->redirectToRoute('app_dashboard');
    }
}
