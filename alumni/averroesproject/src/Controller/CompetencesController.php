<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\CompetencesType;
use App\Entity\Competences;


class CompetencesController extends AbstractController
{
    #[Route('/competences', name: 'app_competences')]
    public function index(Request $request,EntityManagerInterface $entityManager): Response
    {

        $competences = new Competences();
        $form = $this->createForm(CompetencesType::class, $competences);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // On recupere le profil de la personne connecter 
            $acc = $this->getUser()->getId();
            $profilUser = $entityManager->getRepository("App\Entity\Profil")->findOneBy(array('account' => $acc));

           //on set le bon profil et on set la date actuelle
            $competences->setProfil($profilUser);
            // on envoie dans la bdd
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($competences);
            $entityManager->flush();

            // on refresh la page
            return $this->redirectToRoute('app_competences');
            
        }


            //on recupere le profil de la personne connecter
            $monId = $this->getUser()->getId();
            $Monprofil = $entityManager->getRepository("App\Entity\Profil")->findOneBy(array('account' => $monId));

            //on recupere les evenements creer par la personne connecter
            $MesCompetences = $entityManager->getRepository("App\Entity\Competences")->findBy(array('profil' => $Monprofil));


        return $this->render('competences/index.html.twig', [
            'form' => $form->createView(),
            'MesCompetences' => $MesCompetences,
        ]);
    }

    //bouton pour supprimer une competences
    #[Route('/competences/delete/{id}', name: 'competences_delete', methods: ['POST'])]
    public function delete(Competences $competences, EntityManagerInterface $entityManager): RedirectResponse
    {
        $entityManager->remove($competences);
        $entityManager->flush();

        return $this->redirectToRoute('app_competences');
    }
}
