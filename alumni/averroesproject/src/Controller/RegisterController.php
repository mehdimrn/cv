<?php

namespace App\Controller;

use App\Entity\Account;
use App\Entity\Profil;
use App\Form\RegisterType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Repository\Exception\InvalidFindByCall;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\VarDumper\Cloner\Data;

class RegisterController extends AbstractController
{
    #[Route('/inscription', name: 'app_register')]
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager): Response
    {

        
        $form = $this->createForm(RegisterType::class);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form ->getData();

            $user = new Account ;
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('Password')->getData()
                )
            )
            ->setEmail($data['email'])
            ->setRoles($data['roles']);
            
            $entityManager->persist($user);
            $entityManager->flush();
            
            
            $var = $entityManager->getRepository("App\Entity\Account")->findAll();
            $last_account = $var[count($var)-1];

            $profil =new Profil ;
            $profil->setLastName($data['LastName'])
                   ->setFirstName($data['FirstName'])
                   ->setPhoneNumber($data['PhoneNumber'])
                   ->setYearBac($data['yearBac'])
                   ->setStudentClass($data['studentClass'])
                   ->setAccount($last_account);


                  $entityManager->persist($profil);
                  $entityManager->flush();

                  return $this->redirectToRoute('app_login');


        }



        return $this->render('register/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
