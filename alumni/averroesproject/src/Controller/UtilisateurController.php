<?php

namespace App\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Diploma;
use App\Entity\InformationsProfil;
use App\Entity\Profil;
use App\Entity\Competences;
use Symfony\Component\Routing\Annotation\Route;

class UtilisateurController extends AbstractController
{
    #[Route('/utilisateur', name: 'app_utilisateur')]
    public function index(EntityManagerInterface $entityManager ): Response
    {
        $Users = $entityManager->getRepository("App\Entity\Profil")->findAll();

        return $this->render('utilisateur/index.html.twig', [
            'Users' => $Users,
        ]);
    }

    #[Route('/utilisateur/{id}', name: 'app_utilisateur_details')]
    public function showDetails($id, EntityManagerInterface $entityManager): Response
    {
        // pour afficher l'utilisateur 
        $user = $entityManager->getRepository("App\Entity\Profil")->find($id);

        // pour afficher ses info profil
        $infoUser = $entityManager->getRepository("App\Entity\InformationsProfil")->findOneBy(array('profil' => $id));

        //pour afficher ses competences
        $competencesUser = $entityManager->getRepository("App\Entity\Competences")->findBy(array('profil' => $id));

        //pour afficher ses diplomes 
        $diplomeUser = $entityManager->getRepository("App\Entity\Diploma")->findBy(array('profil' => $id));

        // pour afficher le mail et le role
        $roleUser = $user->getAccount()->getRoles();
        $mailUser = $user->getAccount()->getEmail();

        return $this->render('utilisateur/details.html.twig', [
            'user' => $user,
            'infoUser' => $infoUser,
            'diplomeUser' => $diplomeUser,
            'competencesUser' => $competencesUser,
            'roleUser' => $roleUser[0],
            'mailUser' => $mailUser,
        ]);
    }
}
