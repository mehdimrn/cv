<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Account;
use App\Entity\InformationsProfil;
use App\Entity\Profil;

class LoginFromAuthentificatorAuthenticator extends AbstractLoginFormAuthenticator
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = 'app_login';
    private EntityManagerInterface $entityManager;

    public function __construct(private UrlGeneratorInterface $urlGenerator,EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function authenticate(Request $request): Passport
    {
        $email = $request->request->get('email', '');

        $request->getSession()->set(Security::LAST_USERNAME, $email);

        return new Passport(
            new UserBadge($email),
            new PasswordCredentials($request->request->get('password', '')),
            [
                new CsrfTokenBadge('authenticate', $request->request->get('_csrf_token')),
            ]
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $firewallName)) {
            return new RedirectResponse($targetPath);
        }
        // Si l'utilisateur n'a pas d'information profil alors il doit etre rediriger vers app_information_profil sinon il est rediriger vers le dashboard

        //process pour recup les informationsprofil de l'utilisateur qui est en train de se connecter
        $user = $token->getUser();
        $iduser = $user->getId();
        $profilUser = $this->entityManager->getRepository("App\Entity\Profil")->findOneBy(array('account' => $iduser));
        $informationsProfil = $this->entityManager->getRepository("App\Entity\InformationsProfil")->findOneBy(array('profil' => $profilUser));

        // si les infoprofil sont null alors il est rediriger vers app_information_profil
         if ($informationsProfil === null) {
            return new RedirectResponse($this->urlGenerator->generate('app_information_profil'));
        }
        else{

    // Si les InformationsProfil ne sont pas null ou si l'utilisateur n'est pas une instance de Account,
    // redirigez-le vers le tableau de bord ou toute autre page nécessaire.
    return new RedirectResponse($this->urlGenerator->generate('app_dashboard'));
}
    }
  
    protected function getLoginUrl(Request $request): string
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }
}
