<?php

namespace App\Form;

use App\Entity\Diploma;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DiplomaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('yearDiploma')
            ->add('descDiploma')
            ->add('linkDiploma')
            ->add('school')
            ->add('levelDiploma')
            ->add('typeDiploma')
            ->add('submit', SubmitType::class, [
                'label' => "ajouter diplome",
                'attr' => [
                    'class' => "btn-dark w-150 ",
                ]
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Diploma::class,
        ]);
    }
}
