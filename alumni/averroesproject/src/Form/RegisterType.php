<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\CallbackTransformer;



class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $tab=[]
;        for ($i=2003; $i < date('Y'); $i++) { 
            $tab[$i] = $i;
        }
        $tab['eleve actuel']='eleve actuel';
        
        $years = range(date('Y'), 2003);
        $builder

            







            ->add('LastName', TextType::class, [
                'label' => 'Votre nom ',
                'constraints' => new Length([
                    'min' => 2,
                    'max' => 30
                ]),
                'attr' => [
                    'placeholder' => 'Merci de saisir votre nom',
                    'class' => 'bootstrap-class'
                ]
            ])

            ->add('FirstName', TextType::class, [
                'label' => 'Votre prénom ',
                'constraints' => new Length([
                    'min' => 2,
                    'max' => 30
                ]),
                'attr' => [
                    'placeholder' => 'Merci de saisir votre prénom',
                    'class' => 'bootstrap-class'
                ]
            ])


            ->add('email', EmailType::class, [
                'label' => 'Votre email ',
                'constraints' => new Length([
                    'min' => 2,
                    'max' => 60
                ]),
                'attr' => [
                    'placeholder' => 'Merci de saisir votre email',
                    'class' => 'bootstrap-class',
                    'pattern' => "[a-z0-9._%+-]+@[a-z0-9.-]+\.(?:[a-z]{2}|com|org|gouv|net)$"
                ]
            ])

            ->add('PhoneNumber', TelType::class, [
                'label' => 'Votre numéro de téléphone ',
                'constraints' => new Length([
                    'min' => 10,
                    'max' => 10
                ]),
                'attr' => [
                    'placeholder' => 'Merci de saisir votre numéro de téléphone',
                    'class' => 'bootstrap-class'
                ]
            ])

            ->add('yearBac', ChoiceType::class, [
                'choices' => $tab,
                'label' => 'Année obtention bac ',
            ])

            ->add('studentClass', ChoiceType::class, [
                'label' => "Classe de l'éleve ",
                'choices'  => [
                    'ancien eleve' => 'ancien eleve',
                    '6ème' => '6 eme',
                    '5ème' => '5 eme',
                    '4ème' => '4 eme',
                    '3ème' => '3 eme',
                    '2nde' => '2 nd',
                    '1ère' => '1er',
                    'Terminale' => 'Term',
                ],
            ])

            ->add('Password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Le mot de passe et la confirmation doivent être identique.',
                'label' => 'Votre mot de passe ',
                'required' => true,
                'first_options' => [
                    'label' => 'Mot de passe',
                    'attr' => [
                        'placeholder' => 'Merci de saisir votre mot de passe',
                        'class' => 'bootstrap-class'
                    ]
                ],

                'second_options' => [
                    'label' => 'Confirmez votre mot de passe ',
                    'attr' => [
                        'placeholder' => 'Merci de confirmer votre mot de passe',
                        'class' => 'bootstrap-class'
                    ]
                ],

            ])

            ->add('roles', ChoiceType::class, [
                'label' => 'Role ',
                'choices' => [
                'Ancien Élève' => "Ancien Eleve", 
                'Moderateur' => "Moderateur", 
                'Élève Actuel' => "Eleve", 
                'secretaire' => "secretaire",
                ]])
                
            ->add('submit', SubmitType::class, [
                'label' => "S'inscrire",
                'attr' => [
                    'class' => "btn-dark w-150 ",
                ]
            ]);

        $builder->get('roles')
            ->addModelTransformer(new CallbackTransformer(
            function ($rolesAsArray) {
                 return is_array($rolesAsArray) ? count($rolesAsArray): 0;
            },
            function ($rolesAsString) {
                 return [$rolesAsString];
            }
        ));
        }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            //
        ]);
    }
}