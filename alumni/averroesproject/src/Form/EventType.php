<?php

namespace App\Form;

use App\Entity\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\PositiveOrZero;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;




class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titleEvent')
            ->add('descEvent')
            ->add('dateEvent')
            ->add('typeEvent')
            ->add('placeEvent',TextType::class,[
                'label' => 'adresse de levent'
            ])
            ->add('numberOfSitEvent', IntegerType::class, [
                'constraints' => [
                    new NotBlank(),
                    new PositiveOrZero(),
                ],
            ])
            ->add('contactEventOwner',TextType::class,[
                'label' => "contact de l'owner "
            ])
            
            ->add('animatorEvent')
            ->add('durationEvent',TextType::class,[
                'label' => 'duree de levenement'
            ])
            // ->add('PDF')
            ->add('publicPrivateEvent')
            ->add('restricted')
            ->add('freeEvent',TextType::class,[
                'label' => 'Prix de levent'
            ])
            ->add('submit', SubmitType::class, [
                'label' => "Creer l'evenement",
                'attr' => [
                    'class' => "btn-dark w-150 ",
                ]
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
        ]);
    }
}
