<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230511161532 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE account (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, is_verified TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_7D3656A4E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE candidacy_center (id INT AUTO_INCREMENT NOT NULL, profil_id INT DEFAULT NULL, type_candidacy VARCHAR(255) NOT NULL, desc_candidacy LONGTEXT NOT NULL, level_candidacy VARCHAR(255) NOT NULL, duration_candidacy VARCHAR(255) NOT NULL, contact_candidacy VARCHAR(255) NOT NULL, school_candidacy VARCHAR(255) NOT NULL, date_candidacy DATE NOT NULL, rhythme_candidacy VARCHAR(255) NOT NULL, start_candidacy DATE NOT NULL, sector_candidacy VARCHAR(255) NOT NULL, pdf VARCHAR(255) DEFAULT NULL, INDEX IDX_1776BE24275ED078 (profil_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE career_center (id INT AUTO_INCREMENT NOT NULL, profil_id INT DEFAULT NULL, type_ann VARCHAR(255) NOT NULL, desc_ann LONGTEXT NOT NULL, level_ann VARCHAR(255) NOT NULL, duration_ann VARCHAR(255) NOT NULL, contact_ann VARCHAR(255) NOT NULL, date_ann DATE NOT NULL, rhythme_ann VARCHAR(255) NOT NULL, start_ann DATE NOT NULL, sector_ann VARCHAR(255) NOT NULL, pdf VARCHAR(255) DEFAULT NULL, INDEX IDX_421475AC275ED078 (profil_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE competences (id INT AUTO_INCREMENT NOT NULL, profil_id INT DEFAULT NULL, competence VARCHAR(255) NOT NULL, sector VARCHAR(255) NOT NULL, INDEX IDX_DB2077CE275ED078 (profil_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE diploma (id INT AUTO_INCREMENT NOT NULL, profil_id INT DEFAULT NULL, year_diploma VARCHAR(255) NOT NULL, desc_diploma VARCHAR(255) NOT NULL, link_diploma VARCHAR(255) NOT NULL, school VARCHAR(255) NOT NULL, level_diploma VARCHAR(255) NOT NULL, type_diploma VARCHAR(255) NOT NULL, INDEX IDX_EC218957275ED078 (profil_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event (id INT AUTO_INCREMENT NOT NULL, profil_id INT DEFAULT NULL, title_event VARCHAR(255) NOT NULL, desc_event LONGTEXT NOT NULL, date_post_event DATE NOT NULL, date_event DATE NOT NULL, type_event VARCHAR(255) NOT NULL, place_event VARCHAR(255) NOT NULL, number_of_sit_event INT NOT NULL, contact_event_owner VARCHAR(255) NOT NULL, status_event TINYINT(1) NOT NULL, animator_event VARCHAR(255) NOT NULL, duration_event VARCHAR(255) NOT NULL, pdf VARCHAR(255) DEFAULT NULL, public_private_event INT NOT NULL, restricted VARCHAR(255) DEFAULT NULL, free_event INT NOT NULL, INDEX IDX_3BAE0AA7275ED078 (profil_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE informations_profil (id INT AUTO_INCREMENT NOT NULL, profil_id INT DEFAULT NULL, jobs_title VARCHAR(255) DEFAULT NULL, activity_sector VARCHAR(255) DEFAULT NULL, link_job_card VARCHAR(255) DEFAULT NULL, linkedin VARCHAR(255) DEFAULT NULL, link_company VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_AC0D829C275ED078 (profil_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE list_event_user (id INT AUTO_INCREMENT NOT NULL, event_id INT DEFAULT NULL, id_account INT NOT NULL, number_user_public INT NOT NULL, INDEX IDX_4875EE3E71F7E88B (event_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE profil (id INT AUTO_INCREMENT NOT NULL, account_id INT NOT NULL, lastname VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, phone_number VARCHAR(255) NOT NULL, year_bac VARCHAR(255) DEFAULT NULL, student_class VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_E6D6B2979B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE candidacy_center ADD CONSTRAINT FK_1776BE24275ED078 FOREIGN KEY (profil_id) REFERENCES profil (id)');
        $this->addSql('ALTER TABLE career_center ADD CONSTRAINT FK_421475AC275ED078 FOREIGN KEY (profil_id) REFERENCES profil (id)');
        $this->addSql('ALTER TABLE competences ADD CONSTRAINT FK_DB2077CE275ED078 FOREIGN KEY (profil_id) REFERENCES profil (id)');
        $this->addSql('ALTER TABLE diploma ADD CONSTRAINT FK_EC218957275ED078 FOREIGN KEY (profil_id) REFERENCES profil (id)');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA7275ED078 FOREIGN KEY (profil_id) REFERENCES profil (id)');
        $this->addSql('ALTER TABLE informations_profil ADD CONSTRAINT FK_AC0D829C275ED078 FOREIGN KEY (profil_id) REFERENCES profil (id)');
        $this->addSql('ALTER TABLE list_event_user ADD CONSTRAINT FK_4875EE3E71F7E88B FOREIGN KEY (event_id) REFERENCES event (id)');
        $this->addSql('ALTER TABLE profil ADD CONSTRAINT FK_E6D6B2979B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE candidacy_center DROP FOREIGN KEY FK_1776BE24275ED078');
        $this->addSql('ALTER TABLE career_center DROP FOREIGN KEY FK_421475AC275ED078');
        $this->addSql('ALTER TABLE competences DROP FOREIGN KEY FK_DB2077CE275ED078');
        $this->addSql('ALTER TABLE diploma DROP FOREIGN KEY FK_EC218957275ED078');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA7275ED078');
        $this->addSql('ALTER TABLE informations_profil DROP FOREIGN KEY FK_AC0D829C275ED078');
        $this->addSql('ALTER TABLE list_event_user DROP FOREIGN KEY FK_4875EE3E71F7E88B');
        $this->addSql('ALTER TABLE profil DROP FOREIGN KEY FK_E6D6B2979B6B5FBA');
        $this->addSql('DROP TABLE account');
        $this->addSql('DROP TABLE candidacy_center');
        $this->addSql('DROP TABLE career_center');
        $this->addSql('DROP TABLE competences');
        $this->addSql('DROP TABLE diploma');
        $this->addSql('DROP TABLE event');
        $this->addSql('DROP TABLE informations_profil');
        $this->addSql('DROP TABLE list_event_user');
        $this->addSql('DROP TABLE profil');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
