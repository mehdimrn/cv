<?php
/*
ecrire " entre l'âge de l'enfant"
lire A

si A >= 6 et A <=7 alors
ecris "la catégorie de l'enfant est Poussin"
sinon si A >=8 et A<=9 alors
ecris "la catégorie de l'enfant est Pupille"
sinon si A >=10 et A<=11 alors
ecris "la catégorie de l'enfant est Minime"
sinon si A<12 alors
ecris "la catégorie de l'enfant est Cadet"
 

*/
echo "Entre l'âge de l'enfant : ";
$age = trim(fgets(STDIN));

if ($age >= 6 && $age <=7) {
    echo "la catégorie de l'enfant est Poussin";
}
elseif ($age >= 8 && $age <=9 ){
    echo "la catégorie de l'enfant est Pupille";
}
elseif ($age >= 10 && $age <=11 ){
    echo "la catégorie de l'enfant est Minime";
}
elseif ($age >= 12 ){
    echo "la catégorie de l'enfant est Cadet";
}
else {
    echo "l'enfant est trop jeune !!!";
}

?>