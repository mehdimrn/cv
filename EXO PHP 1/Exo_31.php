<?php
/*
Declaration de tableau 

Tableau Voyelle [5]

pour i <-- 0 à 5
    v[a] <-- "a"
    v[e] <-- "e"
    v[i] <-- "i"
    v[o] <-- "o"
    v[u] <-- "u"
    v[y] <-- "y"
fin 
*/

// création d'un tableau simple vide
$voyelle = array ();

// ajout de voyelle dans le tableau
$voyelle [] = 'a';
$voyelle [] = 'e';
$voyelle [] = 'i';
$voyelle [] = 'o';
$voyelle [] = 'u';
$voyelle [] = 'y';

// lecture des voyelles du tableau
echo $voyelle[0];
echo PHP_EOL;
echo $voyelle[1];
echo PHP_EOL;
echo $voyelle[2];
echo PHP_EOL;
echo $voyelle[3];
echo PHP_EOL;
echo $voyelle[4];
echo PHP_EOL;
echo $voyelle[5];
echo PHP_EOL;



?>

