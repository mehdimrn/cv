<?php
/*
// création d'un tableau simple vide
$tableau = array();

// ajout d'éléments dans le tableau
$tableau[] = 'foo';
$tableau[] = 'bar';
$tableau[] = 'baz';

// lecture des éléments du tableau
echo $tableau[0];
echo PHP_EOL;
echo $tableau[1];
echo PHP_EOL;
echo $tableau[2];
echo PHP_EOL;
****************************************************

// création d'un tableau simple vide
$tableau = array();

// ajout d'éléments dans le tableau
$tableau[100] = 'foo';
$tableau[200] = 'bar';
$tableau[300] = 'baz';

// lecture des éléments du tableau
echo $tableau[100];
echo PHP_EOL;
echo $tableau[200];
echo PHP_EOL;
echo $tableau[300];
echo PHP_EOL;
******************************************************
// création d'un tableau simple
$fibonacci = array(0, 1, 2, 3, 5, 8, 13, 21, 34, 55);

// lecture des éléments du tableau
echo $fibonacci[0];
echo PHP_EOL;
echo $fibonacci[1];
echo PHP_EOL;
echo $fibonacci[2];
echo PHP_EOL;
echo $fibonacci[3];
echo PHP_EOL;
echo $fibonacci[4];
echo PHP_EOL;
echo $fibonacci[5];
echo PHP_EOL;
echo $fibonacci[6];
echo PHP_EOL;
echo $fibonacci[7];
echo PHP_EOL;
echo $fibonacci[8];
echo PHP_EOL;
echo $fibonacci[9];
echo PHP_EOL;

**********************************************************

// création d'un tableau associatif (dictionnaire, hash array)
$legumes = array(
    'a' => array(1,1,3,4),
    'b' => 'broccoli',
    'c' => 'carotte',
);

// lecture des éléments du tableau
echo $legumes['a'];
echo PHP_EOL;

echo $legumes['b'];
echo PHP_EOL;

echo $legumes['c'];
echo PHP_EOL;

***********************************************************

/ initialisation d'une variable nommée $i qui servira d'index / compteur
// la boucle for se terminera quand $i vaudra count($liste), c-à-d quand $i sera égal au nombre d'éléments dans la variable $liste
// la variable $i est incrémentée de 1 à chaque tour
for ($i = 0; $i < count($liste); $i++) {
    // tour à tour, on affiche chaque élément de la variable $liste
    echo $liste[$i];
    echo PHP_EOL;
}
*/



?>
